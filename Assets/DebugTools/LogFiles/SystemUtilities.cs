using System;
using System.Diagnostics;
using System.IO;

namespace DebugTools.LogFiles
{
	public static class SystemUtilities
	{
		public static void OpenPathInFileManager(string path)
		{
#if UNITY_EDITOR_WIN || (!UNITY_EDITOR && UNITY_STANDALONE_WIN)
			OpenInWindowsFileExplorer(path);
#elif UNITY_EDITOR_OSX || (!UNITY_EDITOR && UNITY_STANDALONE_OSX)
			OpenInMacFileBrowser(path);
#endif
		}
		
		private static void OpenInWindowsFileExplorer(string path)
		{
			Process.Start(path);
		}
		
		// Open path in Mac Finder
		// https://answers.unity.com/questions/43422/how-to-implement-show-in-explorer.html?childToView=452794#answer-452794
		private static void OpenInMacFileBrowser(string path)
		{
			path = path.Replace("~", "/Users/" + Environment.UserName);
			
			bool openInsidesOfFolder = false;
			
			// try mac
			string macPath = path.Replace("\\", "/"); // mac finder doesn't like backward slashes
			
			if (Directory.Exists(macPath)) // if path requested is a folder, automatically open insides of that folder
			{
				openInsidesOfFolder = true;
			}
			
			if (!macPath.StartsWith("\""))
			{
				macPath = "\"" + macPath;
			}
			if (!macPath.EndsWith("\""))
			{
				macPath = macPath + "\"";
			}
			string arguments = (openInsidesOfFolder ? "" : "-R ") + macPath;
			
			Process.Start("open", arguments);
		}
	}
}
