using DebugTools.LogFiles;
using UnityEngine;

namespace DebugTools
{
	public class LocatePlayerLogButton : MonoBehaviour
	{
		private void Awake()
		{
#if UNITY_STANDALONE
			gameObject.SetActive(true);
#else
			gameObject.SetActive(false);
#endif
		}
		
		public void LocateLogFile()
		{
#if UNITY_STANDALONE
			var path = System.IO.Path.GetDirectoryName(LogFilePathsAPI.GetPlayerLogPath());
			SystemUtilities.OpenPathInFileManager(path);
#endif
		}
	}
}
