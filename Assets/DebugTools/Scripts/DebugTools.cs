﻿using UnityEngine;
using UnityEngine.Assertions;

namespace DebugTools
{
    public class DebugTools : MonoBehaviour
    {
        [SerializeField] private GameObject debugOverlay;

        private void Awake()
        {
            Assert.IsNotNull(debugOverlay);
            
            debugOverlay.SetActive(false);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Maybe you want to open menu on ESC press,
                // but keep in mind player should always be able to quit / restart etc.
                CloseApp();
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                // TODO: reload game / last checkpoint
            }
            
            if (Input.GetKeyDown(KeyCode.F2))
            {
                // TODO: reload game / last checkpoint
                debugOverlay.SetActive(!debugOverlay.activeSelf);
            }
        }

        private void CloseApp()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
        }
    }
}
