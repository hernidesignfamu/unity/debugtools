using UnityEngine;

namespace DebugTools
{
	/// <summary>
	/// Script for taking care of safe areas especially useful for mobile displays
	/// as they can contain different notches etc.
	/// </summary>
	public class SafeAreaCanvas : MonoBehaviour
	{
		private int lastWidth;
		private int lastHeight;
		
		private RectTransform rectTransform;
		public RectTransform RectTransform
		{
			get
			{
				if (rectTransform == null)
					rectTransform = GetComponent<RectTransform>();
				return rectTransform;
			}
		}

		protected void Start()
		{
			lastWidth = Screen.width;
			lastHeight = Screen.height;
			
			UpdateSafeArea();
		}

		private void Update()
		{
			if(lastWidth != Screen.width || lastHeight != Screen.height)
				UpdateSafeArea();
				
			lastWidth = Screen.width;
			lastHeight = Screen.height;
		}

		void UpdateSafeArea()
		{
			RectTransform.anchorMin = Vector2.zero;
			RectTransform.anchorMax = Vector2.one;
		
			RectTransform.offsetMin = new Vector2((Screen.safeArea.xMin / Screen.width) * RectTransform.rect.width, (Screen.safeArea.yMin / Screen.height) * RectTransform.rect.height);
			RectTransform.offsetMax = new Vector2((Screen.safeArea.xMax / Screen.width) * RectTransform.rect.width - RectTransform.rect.width, (Screen.safeArea.yMax / Screen.height) * RectTransform.rect.height - RectTransform.rect.height);
		}
	}
}
