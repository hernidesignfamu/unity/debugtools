using UnityEngine;
using System.Collections;
public static class VersionInfo
{
	public const string companyName = "FAMU";
	public const string productName = "DebugTools";
	public const string bundleVersion = "0.1";
	public const string buildNumber = "1";
}