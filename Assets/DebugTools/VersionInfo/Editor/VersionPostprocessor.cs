﻿using UnityEditor;
using System.IO;
using System;
using UnityEngine;

public class VersionPostprocessor : AssetPostprocessor 
{
    public static void OnPostprocessAllAssets (
        string[] importedAssets,
        string[] deletedAssets,
        string[] movedAssets,
        string[] movedFromAssetPaths) {

        foreach (string str in importedAssets) {
            if(str == "ProjectSettings/ProjectSettings.asset")
            {
                UpdateVersionInfo();
                break;
            }
        }

    }

    public static void UpdateVersionInfo()
    {
        string fileName = "VersionInfo";
        // TODO: you might update this to fit your project structure
        string fullPath =  "Assets/DebugTools/VersionInfo/" + fileName + ".cs";
        
        string content = "";
        
        content += "using UnityEngine;\n";
        content += "using System.Collections;\n";

        content += "public static class " + fileName + "\n{\n";
        content += "\tpublic const string companyName = \""+PlayerSettings.companyName+"\";\n";
        content += "\tpublic const string productName = \""+PlayerSettings.productName+"\";\n";
        content += "\tpublic const string bundleVersion = \""+PlayerSettings.bundleVersion+"\";\n";
        content += "\tpublic const string buildNumber = \""+PlayerSettings.iOS.buildNumber+"\";\n";

        content += "}";
        
        content = content.Replace("\n", Environment.NewLine);
        
        File.WriteAllText(fullPath, content);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
