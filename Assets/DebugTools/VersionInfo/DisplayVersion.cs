﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayVersion : MonoBehaviour
{
	public static string Version
	{
		get { return $"Build v{VersionInfo.bundleVersion} ({VersionInfo.buildNumber})"; }
	}
	
	void OnEnable()
	{
		GetComponent<Text> ().text = Version;
	}

}
